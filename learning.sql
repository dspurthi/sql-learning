CREATE TABLE orders(
    customer_id INT,
    order_id BIGSERIAL NOT NULL PRIMARY KEY,
    employee_id INT,
    order_date VARCHAR(100) NOT NULL,
    required_date VARCHAR(100) NOT NULL,
    shipped_date VARCHAR(100) NOT NULL,
    ship_via VARCHAR(100) NOT NULL,
    freight INT NOT NULL,
    ship_name VARCHAR(30),
    ship_address VARCHAR(100),
    ship_city VARCHAR(50),
    ship_region VARCHAR(50),
    ship_postal_code INT,
    ship_country VARCHAR(30),
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id),
    FOREIGN KEY(employee_id) REFERENCES employees(employee_id),
    FOREIGN KEY (ship_name) REFERENCES customers(contact_name),
    FOREIGN KEY (ship_address) REFERENCES customers(address),
    FOREIGN KEY(ship_city) REFERENCES customers(city),
    FOREIGN KEY(ship_region) REFERENCES customers(region),
    FOREIGN KEY(ship_postal_code) REFERENCES customers(postal_code),
    FOREIGN KEY(ship_country) REFERENCES customers(country)
);

CREATE TABLE order_details (
	order_id INT NOT NULL,
    product_id INT NOT NULL,
    unit_price INT NOT NULL,
    quantity INT NOT NULL,
    discount INT NOT NULL,
    FOREIGN KEY (order_id) REFERENCES orders(order_id),
    FOREIGN KEY (product_id) REFERENCES products(product_id)
);

CREATE TABLE employee_territories(
    employee_id INT NOT NULL,
    territory_id VARCHAR(50) NOT NULL,
    FOREIGN KEY (employee_id) REFERENCES employees(employee_id),
    FOREIGN KEY(territory_id) REFERENCES territories(territory_id)
    );
CREATE TABLE territories(
    territory_id VARCHAR(50) NOT NULL PRIMARY KEY,
    territory_description VARCHAR(100) NOT NULL,
    region_id INT NOT NULL,
    FOREIGN KEY(region_id) REFERENCES region(region_id)
);
CREATE TABLE region(
    region_id INT NOT NULL PRIMARY KEY,
    region_description VARCHAR(50) NOT NULL
);
ALTER TABLE public.customers ALTER COLUMN postal_code TYPE bigint USING postal_code::bigint;
ALTER TABLE public.customers ALTER COLUMN phone TYPE bigint USING phone::bigint;

INSERT INTO public.customers(
	customer_id, company_name, contact_name, contact_title, address, city, region, postal_code, country, phone)
	VALUES (1, 'poochiTextTiles', 'spurthi', 'Mrs', 'wyra', 'khammam', 'southern', 507165, 'IND', 8143542161),
    (2, 'CHINTUTextTiles', 'srimanth', 'Mr', 'mangapuram', 'khammam', 'southern', 507165, 'IND', 9742704854),
    (3, 'shinyTextTiles', 'kruthi', 'Ms', 'khammam',' khammam', 'southern', 507002, 'IND', 9493807158),
    (4, 'vijayaTextTiles', 'vijaya', 'Mrs', 'wyra', 'khammam', 'southern', 507165, 'IND', 9494865920),
    (5, 'chinnyTextTiles', 'keerthi', 'Mrs', 'khammam', 'khammam', 'southern', 507002, 'IND', 9030133646);


INSERT INTO public.employees
(employee_id, last_name, first_name, title, title_of_courtesy, birth_date, hire_date, address, city, region, postal_code, country, home_phone)
VALUES(1, 'srikanth', 'duggineni', 'Mr', '', '25-10-1990', '09-02-2020', 'mangapuram', 'khammam', 'SOUTH', 507165, 'IND', 9785461538),
(2, 'rakesh', 'manukonda', 'Mr', '', '06-04-1986', '25-04-2012', 'khammam', 'khammam', 'SOUTH', 507002, 'IND', 9948834892);



INSERT INTO public.orders
(customer_id, order_id, employee_id, order_date, required_date, shipped_date, ship_via, freight, ship_name, ship_address, ship_city, ship_region, ship_postal_code, ship_country)
VALUES(4, 2, 1, '31-03-2021', '04-04-2021', '01-04-2021', 'XPRESSBEES', 50, 'nirmala', 'cubixsaphire', 'secunderabad', 'southwest', 500082, 'IND');

INSERT INTO public.suppliers
(supplier_id, company_name, contact_name, contact_title, address, city, region, postal_code, country, phone, homepage)
VALUES(101, 'prestige', 'suresh', 'Mr', 'yapral', 'shailigardenia', 'sothwest', '500085', 'USA', '9949012109', 'www.prestige.com'),
(102, 'homecoocking', 'kamala', 'Mrs', 'keesara', 'gardentowers', 'sotheast', '507001', 'UK', '9848448483', 'www.homecoocking.com');

INSERT INTO public.products
(product_id, product_name, supplier_id, category_id, quantity_per_unit, unit_price, units_in_stock, units_on_order, reorder_level, discontinued)
VALUES(11001, 'frypan', 102, 1, '1', '300', '3', '1', 2, 1),
(11002, 'kadai', 101, 1, '1', '750', '4', '2', 2, 2);

INSERT INTO public.order_details
(order_id, product_id, unit_price, quantity, discount)
VALUES(1, 11002, 750, 1, 10),
(2, 11001, 300, 1, 20);

INSERT INTO public.shippers
(shipper_id, company_name, phone)
VALUES(201, 'ecom', 8569472137),
(202, 'XPRESSBEES', 9874587469);

SELECT orders.order_id, customers.contact_name ,orders.employee_id,employees.last_name,employees.first_name,order_details.product_id,products.product_name,products.unit_price, orders.required_date,orders.ship_via,orders.ship_address 
FROM orders 
INNER JOIN customers
ON orders.customer_id =customers.customer_id
inner JOIN employees on employees.employee_id = orders.employee_id 
left join order_details on order_details.order_id = orders.order_id 
left join products on order_details.product_id = products.product_id ;


    
