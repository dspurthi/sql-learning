CREATE TABLE aviation.airlines (
    id bigint not null primary key,
	name_of_airlines varchar(200) NOT NULL,
	alias varchar(100) NULL,
	iata varchar(10) NULL,
	icao varchar(20) NULL,
	callsign varchar(200) NULL,
	country varchar(100) NULL,
	active varchar(50) NULL
);

COPY aviation.airlines(id,name_of_airlines,alias,iata,icao,callsign,country,active)
FROM 'C:\Users\spurt\Desktop\learning\data\airlines.csv'
DELIMITER ','
CSV HEADER;

CREATE TABLE aviation.routes (
    airline varchar(100),
    airline_id varchar(150),
    source_airport varchar(200),
	sourceairport_id varchar(200),
	destination_airport varchar(100) NULL,
	destination_airport_id varchar(150) NULL,
	codeshare varchar(20) NULL,
	stops varchar(200) NULL,
	equipment varchar(100) NULL
);

COPY aviation.routes(airline,airline_id,source_airport,sourceairport_id,destination_airport,destination_airport_id,codeshare,stops,equipment)
FROM 'C:\Users\spurt\Desktop\learning\data\routes.csv'
DELIMITER ','
CSV HEADER;

CREATE table aviation.planes(
	name VARCHAR(300),
	icao VARCHAR(50),
	iata VARCHAR(50)
);

COPY aviation.planes(name,icao,iata)
FROM 'C:\Users\spurt\Desktop\learning\data\planes.csv'
DELIMITER ','
CSV HEADER;

CREATE TABLE aviation.countries(
	name VARCHAR(200),
	iso_code VARCHAR(50),
	dafif_code VARCHAR(50)
);

COPY aviation.countries(name,iso_code,dafif_code)
FROM 'C:\Users\spurt\Desktop\learning\data\planes.csv'
DELIMITER ','
CSV HEADER;



